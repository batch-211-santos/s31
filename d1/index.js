// NODE JS INTRODUCTION

// Use the "require" directive to load Node.js modules (http)
//  A module is a software component or part of a program that contains one or more routines
// The "http module" lets node.js transfer data using the HyperText transfer protocol
// The "http module" is a set of individual files that contain code to create "component" that helps establish data transfer between applications
// HTTP is a protocol that allows the fetching of resources such as HTML documents
// Clients (browsers) and servers (Node.JS/Express.JS applications) communicate by exchanging individual messages 
// The messages sent by the client, usually, a Web Browser, are called requests
// The messages sent by the server as an answer called responses

let http = require("http");


// The http module has a createServer method that accepts a function as an argument and allows for a creation of a server
// The arguments passed in the function are request and response objects that contains methods that allow us  to receive requests from the client and send responses back to it 

http.createServer(function (request, response) {

	/*
		Use the writeHead() method to:
		-Set a status code for the response- a 200 means OK
	*/
	response.writeHead(200, {'Content-type': 'text/plain'}),

	// Send the response with a text content "Hello World"
	response.end('Hello World');

}).listen(4000)


// A port is a virtual point where network connections start and end

// Each port is associated with a specific process or service

// The server will be assigned to port 4000 via the "listen(4000)" method where the server will listen to any requests that are sent to it, eventually communication with our server.


console.log('Server running at localhost:4000');